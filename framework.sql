-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 12 jan. 2021 à 10:24
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `framework`
--

-- --------------------------------------------------------

--
-- Structure de la table `module_intro_slides`
--

DROP TABLE IF EXISTS `module_intro_slides`;
CREATE TABLE IF NOT EXISTS `module_intro_slides` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `module_intro_slides`
--

INSERT INTO `module_intro_slides` (`id`, `image`, `title`, `subtitle`, `text`) VALUES
(26, '5fca5009e0089.jpeg', 'hich', 'kotti', ''),
(27, '5fca50293a85a.jpeg', 'top model', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `username` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `added_by` mediumint(8) UNSIGNED DEFAULT NULL,
  `activation_key` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `role` varchar(20) CHARACTER SET latin1 NOT NULL,
  `mobile` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `nom` varchar(20) CHARACTER SET latin1 NOT NULL,
  `prenom` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `added_by` (`added_by`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `added_date`, `added_by`, `activation_key`, `role`, `mobile`, `nom`, `prenom`) VALUES
(72, 'email@domain.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2016-10-17 11:59:45', NULL, '', 'root', '', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `users_forget`
--

DROP TABLE IF EXISTS `users_forget`;
CREATE TABLE IF NOT EXISTS `users_forget` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `forget_date` varchar(100) CHARACTER SET latin1 NOT NULL,
  `forget_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `forget_key` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users_try`
--

DROP TABLE IF EXISTS `users_try`;
CREATE TABLE IF NOT EXISTS `users_try` (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) UNSIGNED DEFAULT NULL,
  `date_try` varchar(50) CHARACTER SET latin1 NOT NULL,
  `ip` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  `try_type` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
