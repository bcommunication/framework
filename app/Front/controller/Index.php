<?php

namespace app\Front\controller;

class Index
{
    public function __construct()
    {
        user_role();
    }

    /**
     * @front_accueil_url [get,post]:/
     */
    public function main()
    {
        if (http_method('post')) {

        } else {
            $slides = get_sql()->sql('SELECT * FROM module_intro_slides WHERE 1;')->findMany();
            $info = [];
            view_load('front.index', ['info'=>$info, 'slides' => $slides]);
        }
    }
}