<style>
    .tel{
        position: absolute;
        color:white;
        font-weight: 400;
    }
    @media (min-width: 992px) {
        .mediaCondition {
            margin-left: 100px
        }
        .tel2 {
            display: none;
        }
    }
    @media (max-width: 991px) {
        .tel {
            display: none;
        }
        .tel2 {
            color: white;
            font-weight: 400;
            display: initial;
        }
    }
    @media (min-width: 992px){
        .dropdown-menu .dropdown-toggle:after{
            border-top: .3em solid transparent;
            border-right: 0;
            border-bottom: .3em solid transparent;
            border-left: .3em solid;
        }
        .dropdown-menu .dropdown-menu{
            margin-left:0; margin-right: 0;
        }
        .dropdown-menu li{
            position: relative;
        }
        .nav-item .submenu{
            display: none;
            position: absolute;
            left:100%; top:-7px;
        }
        .nav-item .submenu-left{
            right:100%; left:auto;
        }
        .dropdown-menu > li:hover{ background-color: #f1f1f1 }
        .dropdown-menu > li:hover > .submenu{
            display: block;
        }
    }
</style>
<div class="collapse navbar-collapse" id="section-nav">
    <ul class="navbar-nav">
        <li class="nav-item <?= route_name()=='front_accueil'?'active':'' ?>"><a href="" class="nav-link">Accueil</a></li>
        <li class="mediaCondition nav-item <?= route_name()=='front_accueil'?'active':'' ?>"><a href="" class="nav-link">Femme</a></li>
        <li class="nav-item <?= route_name()=='front_menu'?'active':'' ?>"><a href="" class="nav-link">Homme</a></li>
        <li class="nav-item <?= route_name()=='front_order'?'active':'' ?>"><a href="" class="nav-link">Best Basic</a></li>
        <li class="nav-item <?= route_name()=='front_order'?'active':'' ?>"><a href="" class="nav-link">Outlet</a></li>
        <li class="nav-item <?= route_name()=='front_order'?'active':'' ?>"><a href="" class="nav-link">La marque</a></li>
        <li class="mediaCondition nav-item <?= route_name()=='front_accueil'?'active':'' ?>"><a href="" class="nav-link"><i class="fa fa-search"></i></a></li>
        <li class="nav-item <?= route_name()=='front_accueil'?'active':'' ?>"><a href="" class="nav-link"><i class="fa fa-map-marker"></i></a></li>
        <li class="nav-item <?= route_name()=='front_accueil'?'active':'' ?>"><a href="" class="nav-link"><i class="fa fa-user"></i></a></li>
        <li class="nav-item <?= route_name()=='front_accueil'?'active':'' ?>"><a href="" class="nav-link"><i class="fa fa-shopping-basket"></i></a></li>
    </ul>
</div>