<style>
    p{
        margin: 0;
    }
</style>
<div class="overlay"></div>
<div class="r1">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="">
                    <?= $info?$info->horaire:''; ?>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="r2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="">
                    <?= $info?$info->addr:''; ?>
                </span>
            </div>
            <div class="r3" style="position: absolute;right: 10px;bottom: 13px;">
                <a href="<?= $info?$info->lien:'#'; ?>"><span class="icon-facebook"></span></a>
            </div>
        </div>
    </div>
</div>
