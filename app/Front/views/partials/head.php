	<title>Magline</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Restaurant Chinois ">
    <meta name="keywords" content="Restaurant Aline tahiti, Snack restaurant chinois">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/framework/res/assets/front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/open-iconic-bootstrap.css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/animate/animate_2015.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/owl.carousel/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/owl.carousel/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/aos/css/aos.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/ionic.css/ionicons.min.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/bootstrap/css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/jquery/css/jquery.timepicker.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/flaticon.css/flaticon.css">
	<link rel="stylesheet" href="/framework/res/assets/front/vendor/icomoon.css/icomoon.css">
	<link rel="stylesheet" href="/framework/res/assets/front/css/style.css?<?= uniqid(); ?>">
