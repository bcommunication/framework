<!DOCTYPE html>
<html lang="en">
<head>
    <?php view_part('Front.partials.head') ?>
</head>
<style>
    @media (max-width: 991px) {
        .owl-carousel.home-slider {
            /*height: 578px;*/
        }
        .new .owl-carousel .slider-item {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
            height: 400px;
            width: 100%!important;
            position: relative;
            z-index: 0;
        }
        #news .pretitle{
            padding: 16px;
        }
    }
    @media (min-width: 992px) {
        .owl-carousel.home-slider {
            /*height: 737px;*/
        }
    }
    #separation{
        height: 150px;
    }
    #news .pretitle{
        font-family: HelveticaBold,sans-serif;
        color: rgb(0, 0, 0);
    }
    .new .owl-carousel .slider-item {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        height: 400px;
        width: 320px;
        position: relative;
        z-index: 0;
    }
</style>
<body>
    <div class="fixed-top">
        <nav class="navbar navbar-expand-lg navbar-dark custom_navbar bg-dark section-navbar-light" id="section-navbar">
            <div class="container" style="max-width: 1825px !important;">
                <a class="navbar-brand" href="#"><img height="60;" src="/framework/res/assets/img/logoPH.png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#section-nav" aria-controls="section-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"> Menu</span>
                </button>
                <?php view_part('front.partials.navbar',['info'=>$info]) ?>
            </div>
        </nav>
    </div>
	<section class="home-slider owl-carousel">
        <?php if(isset($slides)&&$slides)  : ?>
		<?php foreach ($slides as $slide) : ?>
			<div class="slider-item" style="background-image: url(/framework/res/assets/img/slide/<?= $slide->image; ?>);margin-right: 16px;margin-left: 16px;">
                <div class="container">
                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                        <div class="col-md-7 col-sm-12 text-center section-animate">
                            <?php if ($slide->title) {
                                echo '<span class="subheading">' . $slide->title . '</span>';
                            }
                            if ($slide->subtitle) {
                                echo '<h1 class="mb-4">' . $slide->subtitle . '</h1>';
                            }
                            if ($slide->text) {
                                echo '<p class="mb-4 mb-md-5">' . $slide->text . '</p>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
			</div>
		<?php endforeach;endif; ?>
	</section>
    <main class="inner-main" style="margin-top: 20px;">
        <div class="container" style="max-width: 1825px !important;">
            <div class="row">
            </div>
            <section id="menu-layout">
                <div class="row isotopSet">
                    <?php if(true) : ?>
                                <div class="col-xs-3 col-md-3 product-card menu-item">
                                    <div class="card mb-3 shadow-sm">
                                        <?php if (true) : ?>
                                            <img class="card-img-top"
                                                 src="/framework/res/assets/img/DSC_2840.jpg">
                                        <?php endif ?>
                                        <div class="card-body" style="padding-top: 0">
                                            <h3 data-type=""></h3>
                                            <p class="card-text"></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <button type="submit" data-id="1"
                                                        class="btn btn-yellow add_to_cart">Je découvre
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-3 product-card menu-item">
                                    <div class="card mb-3 shadow-sm">
                                        <?php if (true) : ?>
                                            <img class="card-img-top"
                                                 src="/framework/res/assets/img/PK2820H R copy.jpg">
                                        <?php endif ?>
                                        <div class="card-body" style="padding-top: 0">
                                            <h3 data-type=""></h3>
                                            <p class="card-text"></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <button type="submit" data-id="1"
                                                        class="btn btn-yellow add_to_cart">Je découvre
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-3 product-card menu-item">
                                    <div class="card mb-3 shadow-sm">
                                        <?php if (true) : ?>
                                            <img class="card-img-top"
                                                 src="/framework/res/assets/img/DSC_2856.JPG">
                                        <?php endif ?>
                                        <div class="card-body" style="padding-top: 0">
                                            <h3 data-type=""></h3>
                                            <p class="card-text"></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <button type="submit" data-id="1"
                                                        class="btn btn-yellow add_to_cart">Je découvre
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-3 product-card menu-item">
                                    <div class="card mb-3 shadow-sm">
                                        <?php if (true) : ?>
                                            <img class="card-img-top"
                                                 src="/framework/res/assets/img/3A7A4774.JPG">
                                        <?php endif ?>
                                        <div class="card-body" style="padding-top: 0">
                                            <h3 data-type=""></h3>
                                            <p class="card-text"></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <button type="submit" data-id="1"
                                                        class="btn btn-yellow add_to_cart">Je découvre
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <?php endif; ?>
                </div>
            </section>
            <section id="menu-layout new">
                <div class="row" id="news"><h2 class="">
                        <span class="pretitle">NOUVEAUTÉS</span>
                        <span class="title"></span></h2>
                </div>
                <div class="row new" style="  align-items: center;justify-content: center;">
                        <div class="owl-carousel owl-theme">
                            <div class="slider-item" style="background-image: url(/framework/res/assets/img/3A7A6006.jpg);">
                                <div class="container">
                                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                                        <div class="col-md-7 col-sm-12 text-center section-animate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item" style="background-image: url(/framework/res/assets/img/3A7A5694.jpg);">
                                <div class="container">
                                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                                        <div class="col-md-7 col-sm-12 text-center section-animate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item" style="background-image: url(/framework/res/assets/img/3A7A5661copycopy.jpg);">
                                <div class="container">
                                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                                        <div class="col-md-7 col-sm-12 text-center section-animate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item" style="background-image: url(/framework/res/assets/img/3A7A6052.png);">
                                <div class="container">
                                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                                        <div class="col-md-7 col-sm-12 text-center section-animate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-item" style="background-image: url(/framework/res/assets/img/3A7A5711-Recovered.jpg);">
                                <div class="container">
                                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                                        <div class="col-md-7 col-sm-12 text-center section-animate">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
            <div id="separation">
            </div>
        </div>
    </main>
    <footer class="section-footer fixed-bottom img">
		<?php view_part('front.partials.footer',['info'=>$info]); ?>
	</footer>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="/framework/res/assets/front/vendor/popper/js/popper.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.easing.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.waypoints.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.stellar.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.timepicker.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.animateNumber.min.js"></script>
	<script src="/framework/res/assets/front/vendor/jquery/js/jquery.magnific-popup.min.js"></script>
	<script src="/framework/res/assets/front/vendor/owl.carousel/js/owl.carousel.min.js"></script>
	<script src="/framework/res/assets/front/vendor/aos/js/aos.js"></script>
	<script src="/framework/res/assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/framework/res/assets/admin/plugins/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.js"></script>
    <script src="/framework/res/assets/front/vendor/scrollax/scrollax.min.js"></script>
	<script src="/framework/res/assets/front/vendor/php-validate-form/validate.js"></script>
	<script type="text/javascript" src="/framework/res/assets/front/js/main.js"></script>
	<script>
		$(document).ready(() => {
			var url = window.location.href;
			if (url.indexOf("#") > 0) {
				var activeTab = url.substring(url.indexOf("#") + 1);
				$('.nav[role="tablist"] a[href="#' + activeTab + '"]').tab('show');
				var position = $("#menu").offset().top - $(".navbar").height();
				$("html, body").animate({
					scrollTop: position
				}, 1000);
			}
		});
		// Events carousel
		/*$(".events-carousel").owlCarousel({
			autoplay: true,
			dots: true,
			loop: true,
			items: 1,
			autoplayTimeout: 10000
		});*/
        $('.owl-carousel').owlCarousel({
            autoplay: true,
            dots: false,
            loop: true,
            margin:10,
            items: $(window).width() > 991?5:1,
            autoplayTimeout: 5000
        });

        $('.owl-carousel').trigger('refresh.owl.carousel');

    </script>
</body>
</html>