<?php
namespace app;
class Parameter 
{
    //Maher
    //Config DB
    const DB_DRIVER = 'mysql';
    const DB_NAME = 'framework';
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = '';

    public static $DB_CONNECTION_OPTIONS = [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',\PDO::ATTR_PERSISTENT         => true];
    
    public static function first()
    {
        if(http_post('cookie'))
        {
            session_id(http_post('cookie'));
        }
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credentials: true');
    }
    
    public static function after()
    {

    }

    public static function error(\Exception $e)
    {
        
        http_response_code(404);
        view_load('admin.404');
    }

    const BASE_DIR = '/framework';
    public static $acl = [
        
    ];
    
}
