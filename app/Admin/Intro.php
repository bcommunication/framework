<?php

namespace app\Admin;

/**
 * Description of Intro
 *
 * @author maher
 */
class Intro
{

    public function __construct()
    {
        if (!user_is_root()) {
            throw new \Falf\NotFoundException;
        }
    }

    /**
     * @admin_slider_url [get,post]:/intro
     */
    public function main()
    {
        if (http_method('post')) {
            $do = http_post('do');
            if ($do && $do != 'main' && method_exists($this, $do)) {
                call_user_func([$this, $do]);
            }
        } else {
            $carousel = get_sql()->sql('SELECT * FROM module_intro_slides WHERE 1;')->findMany();
            view_load('admin.intro.main', ['carousel' => $carousel]);
        }
    }

    // Carousel section
    private function getAddForm()
    {
        json_response(['html' => view_content('admin.intro.ajax.add')]);
    }

    private function getUpdateSlideForm()
    {
        if (!filter_var(http_post('id'), FILTER_VALIDATE_INT)) {
            json_response(['html' => view_content('admin.intro.ajax.update_slide', ['slide' => FALSE])]);
        }
        json_response(['html' => view_content('admin.intro.ajax.update_slide', ['slide' => get_sql()->sql('SELECT * FROM module_intro_slides WHERE id = ' . http_post('id'))->find()])]);
    }

    private function addSlide()
    {
        if (!isset($_FILES['image']['tmp_name']) || !file_exists($_FILES['image']['tmp_name'])) {
            json_response(['ok' => FALSE, 'msg' => 'Image obligatoire']);
        }

        $path = $_FILES['image']['tmp_name'];
        $mime = mime_content_type($path);

        if (!in_array($mime, ['image/jpeg', 'image/png', 'image/gif'])) {
            json_response(['ok' => FALSE, 'msg' => 'Format image doit etre jpeg,jpg,png ou gif']);
        }
        $name = uniqid() . '.' . explode('/', $mime)[1];
        $upload_path = PROJECT_DIR . DS . 'res' . DS . 'assets' . DS . 'img' . DS . 'slide' . DS . $name;

        move_uploaded_file($path, $upload_path);
        if (!file_exists($upload_path)) {
            json_response(['ok' => FALSE, 'msg' => 'Erreur de téléchargement de l\'image']);
        }
        $data = [
            'image' => $name,
            'title' => http_post('title'),
            'subtitle' => http_post('subtitle'),
            'text' => http_post('text')
        ];

        $i = get_sql()->forTable('module_intro_slides')->insert($data);

        if (!$i) {
            unlink($upload_path);
            json_response(['ok' => FALSE, 'msg' => 'Erreur ajout slide', 'data' => $data]);
        }
        session_set('msg', 'Slide ajoutée');
        json_response(['ok' => TRUE]);
    }

    private function updateSlide()
    {
        $v = new \Falf\Validator();
        $v->setRules([
            'id' => 'required|int'
        ])->validate();
        $slide = get_sql()->sql('SELECT image FROM module_intro_slides WHERE id = ' . http_post('id'))->find();
        if (!isset($slide->image) || !$slide->image) {
            json_response(['ok' => FALSE, 'msg' => 'Erreur inconnu']);
        }
        if (!$v->isValid()) {
            json_response(['ok' => FALSE, 'msg' => 'Formulaire invalide']);
        }
        $data = [
            'title' => http_post('title'),
            'subtitle' => http_post('subtitle'),
            'text' => http_post('textUpdate')
        ];

        if (isset($_FILES['image']['tmp_name']) && $_FILES['image']['tmp_name'] && file_exists($_FILES['image']['tmp_name'])) {
            $tmp_path = $_FILES['image']['tmp_name'];
            $mime = mime_content_type($tmp_path);
            if (!in_array($mime, ['image/jpeg', 'image/png', 'image/gif'])) {
                json_response(['ok' => FALSE, 'msg' => 'Format d\'image invalide']);
            }
            $name = uniqid() . '.' . explode('/', $mime)[1];
            $data['image'] = $name;
            $new = PROJECT_DIR . DS . 'res' . DS . 'assets' . DS . 'img' . DS . 'slide' . DS . $name;
            $old = PROJECT_DIR . DS . 'res' . DS . 'assets' . DS . 'img' . DS . 'slide' . DS . $slide->image;
        }

        $up = get_sql()->forTable('module_intro_slides')->update($data)->where(['id' => http_post('id')])->run();
        if ($up === 0) {
            json_response(['ok' => FALSE, 'msg' => 'Vous n\'avez rien changé']);
        } else if (!$up) {
            json_response(['ok' => FALSE, 'msg' => 'Erreur modification slide']);
        }
        if (isset($name) && unlink($old)) {
            move_uploaded_file($tmp_path, $new);
        }
        session_set('msg', 'Slide modifiée');
        json_response(['ok' => TRUE]);
    }

    private function deleteSlide()
    {
        if (!filter_var(http_post('id'), FILTER_VALIDATE_INT)) {
            json_response(['ok' => FALSE]);
        }
        $img = get_sql()->sql('SELECT * FROM module_intro_slides WHERE id = ' . http_post('id'))->find();
        if (!$img) {
            json_response(['ok' => FALSE]);
        }
        unlink(PROJECT_DIR . DS . 'res' . DS . 'assets' . DS . 'img' . DS . 'slide' . DS . $img->image);
        get_sql()->exec('DELETE FROM module_intro_slides WHERE id = ' . http_post('id'));
        json_response(['ok' => TRUE]);
    }
}
