<?php



namespace app\Admin;

class Users {
    
    

    /**
     * @login_url [get,post]:/users/login
     */
    public function login()
    {
        if(!user_is_guest())
        {
            throw new \Falf\NotFoundException;
        }
        $login = new \Falf\Users\Account\Login();
        $login->logUser()->by(['username','email','mobile'])->provider('email')->maxTry(3)->view('admin.users.login',['get'])
                ->onSuccess(function($user){
                    route_rewrite('dashboard');
                })->run();
    }
    
    /**
     * @add_user_url [post]:/add-user
     */
    public function add()
    {
        if(!user_is_root())
        {
            throw new \Falf\NotFoundException;
        }
        $v = new \Falf\Validator();
        $v->setRules([
            'nom'=>'required|alpha',
            'prenom'=>'required|alpha',
            'email'=>'required|email',
            'username'=>'required|alnum',
            'pass'=>'required|minlen:6',
            'repass'=>'required|equal:.pass',
            'role'=>'required|in:[root comercial]'
        ])->validate();
        if(!$v->isValid())
        {
            json_reponse(['ok'=>FALSE,'msg'=>'Formulaire invalide',$v->getErrors()]);
        }
        $inserted = get_sql()->forTable('users')->insert([
            'nom'=> http_post('nom'),
            'prenom'=> http_post('prenom'),
            'email'=> http_post('email'),
            'username'=> http_post('username'),
            'password'=> user_crypt_pass(http_post('pass')),
            'added_by'=> user_account('id'),
            'role'=> http_post('role') 
        ]);
        if(!$inserted)
        {
            json_reponse(['ok'=>FALSE,'msg'=>'Email ou nom d\'utilisateur existe']);
        }
        json_reponse(['ok'=>TRUE,'msg'=>'Utilisateur ajouté','html'=> view_content('admin.users.ajax.add_user',['user'=>['nom'=> http_post('nom'),'prenom'=> http_post('prenom'),'username'=> http_post('username'),'email'=> http_post('email'),'role'=> 'ADMIN','id'=>$inserted]])]);
    }
    /**
     * @delete_user_url [post]:/delete-user
     */
    public function delete()
    {
        
        if(!user_is_root())
        {
            throw new \Falf\NotFoundException;
        }
        if(!filter_var(http_post('id'),FILTER_VALIDATE_INT))
        {
            json_reponse(['ok'=>FALSE,'msg'=>'Probléme au niveau de securité']);
        }
        $deleted = get_sql()->sql('DELETE FROM users WHERE id = '.http_post('id').' AND id != '.user_account('id').' AND role != "root"')->run();
        if(!$deleted)
        {
            json_reponse(['ok'=>FALSE,'msg'=>'Vous n\'avez pas la permission pour supprimer cet utilisateur']);
        }
        json_reponse(['ok'=>TRUE,'msg'=>'Utilisateur supprimé']);
    }
    /**
     * @browse_users_url [get]:/browse-users
     */
    public function browse()
    {
        if(!user_is_root())
        {
            throw new \Falf\NotFoundException;
        }
        $page= filter_var(http_get('page'),FILTER_VALIDATE_INT)?http_get('page'):1;
        
        $query = 'SELECT role,nom,prenom,id,username,email FROM users WHERE id!= '. user_account('id');
        if(http_get('q'))
        {
            $q = get_sql()->quote('%'.http_get('q').'%');
            $query.=' AND ( nom like '.$q.' OR prenom like '.$q.' OR username like '.$q.') ';
        }
        $users = get_sql()->sql($query.' ORDER BY id DESC')->paginate(10, $page)->findMany();
        json_reponse(['users'=> view_content('admin.users.ajax.browse',['users'=>$users['data']]),'paging'=>view_content('admin.users.ajax.paging',['paging'=>$users['paging']])]);
    }

    

    /**
     * @manage_users_url [get]:/users
     */
    public function manage()
    {
        if(!user_is_root())
        {
            throw new \Falf\NotFoundException;
        }
        view_load('admin.users.users');
       
    }
    
    /**
     * @users_settings_url [get,post]:/users/settings
     */
    public function settings()
    {
        if(user_is_guest())
        {
            throw new \Falf\NotFoundException;
        }
        if(http_method('post'))
        {
            if(http_post('do') == 'username')
            {
                $v = new \Falf\Validator();
                $v->setRules([
                    'nom'=>'required|alpha',
                    'prenom'=>'required|alpha',
                    'email'=>'required|email'
                ])->validate();
                if(!$v->isValid())
                {
                    json_reponse(['ok'=>FALSE]);
                }
                $sql = new \Falf\sql();
                $ok = $sql->forTable(user_table())->update(['username'=> http_post('username'),'nom'=> http_post('nom'),'prenom'=> http_post('prenom'),'email'=> http_post('email')])->where(['id'=> user_from_session('id')])->run();
                json_reponse(['ok'=>$ok]);
            }
            elseif(http_post('do') == 'password')
            {
                $v = new \Falf\Validator();
                $v->setRules([
                    'old'=>'required',
                    'new'=>'required|minlen:6',
                    'repass'=>'required|equal:.new'
                ])->validate();
                if(!$v->isValid())
                {
                    json_reponse(['ok'=>FALSE,$v->getErrors()]);
                }
                $sql = new \Falf\sql();
                $ok = $sql->forTable(user_table())->update([
                    'password'=> user_crypt_password(http_post('new'))
                ])->where(['id'=> user_id(),'password'=> user_crypt_password(http_post('old'))])->run();
                json_reponse(['ok'=>$ok]);
            }
            elseif(http_post('do') == 'emails')
            {
                $emails = array_filter(explode(',', (string)http_post('emails')));
                if(!$emails)
                {
                    json_reponse(['ok'=>FALSE]);
                }
                $emails_db = [];
                array_walk($emails, function(&$email) use(&$emails_db){
                    if(!filter_var($email,FILTER_VALIDATE_EMAIL))
                    {
                        json_reponse(['ok'=>FALSE]);
                    }
                    $emails_db[] = ['email'=>$email];
                });
                get_sql()->exec('DELETE FROM email_seuil ');
                get_sql()->forTable('email_seuil')->insertGroupeIgnore($emails_db);
                json_reponse(['ok'=>TRUE]);
            }
            
            
        }
        else
        {
            view_load('admin.users.setting');
        }
        
    }
    
    /**
     * @logout_url [get,post]:/users/logout/:token
     */
    public function logout($token)
    {
        if(user_is_guest())
        {
            throw new \Falf\NotFoundException;
        }
        $l = new \Falf\Users\Account\Logout();
        $l->token($token)->onFail(function(){
            throw new \Falf\NotFoundException;
        })->onSuccess(function(){
            route_rewrite('login');
        })->run();
    }
    
    /**
     * @dashboard_url [get,post]:/admin
     */
    public function dashboard()
    {
        if(user_is_guest())
        {
            route_rewrite('login');
        }
        view_load('admin.users.dashboard');
    }
}
