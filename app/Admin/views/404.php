<!DOCTYPE html>
<html class="backend">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>tableau de bord</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/bootstrap.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/layout.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/uielement.css">
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/modernizr/js/modernizr.js"></script>
    </head>
    <body>
        <?php if(!user_is_guest()): ?>
            <?php view_part('admin_sidebar') ?>
        <?php endif; ?>
        <section id="main" role="main">
            <div class="container-fluid"> 
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-minimal" style="margin-top:10%;">
                            <div class="panel-body text-center">
                                <i class="ico-file-remove2 longshadow fsize112 text-default"></i>
                            </div>
                            <div class="panel-body text-center">
                                <h1 class="semibold longshadow text-center text-default fsize32 mb10 mt0">WHOOPS!!</h1>
                                <h4 class="semibold text-primary text-center nm">Page introuvable</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/vendor.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/core.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/app.js"></script>
    </body>
</html>
