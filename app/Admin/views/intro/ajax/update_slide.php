<?php if ($slide) : ?>
    <form name="updateSlider" enctype="multipart/form-data" class="form-horizontal form-bordered" method="POST">
        <div id="update_msg"></div>
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-2">Titre</label>
                <div class="col-sm-10">
                    <input name="title" type="text" class="form-control" value="<?php echo $slide->title; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sous-titre</label>
                <div class="col-sm-10">
                    <input name="subtitle" type="text" class="form-control" value="<?php echo $slide->subtitle; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Texte</label>
                <div class="col-sm-10">
                    <textarea name="textUpdate" class="form-control">
                        <?php echo $slide->text ?>
                    </textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Image</label>
                <div class="col-sm-10">
                    <input type="file" name="image" class="form-control">
                    <input type="hidden" name="do" value="updateSlide" />
                    <input type="hidden" name="id" value="<?php echo $slide->id; ?>" />
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-inverse">Envoyer</button>
        </div>
    </form>
<?php else : ?>
    <div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Erreur inconnu!</strong>
    </div>
<?php endif; ?>
<script>
    CKEDITOR.replace('textUpdate', {
        extraPlugins: 'colorbutton,panelbutton,floatpanel,panel,justify,font'
    });
    $('form[name="updateSlider"]').submit(function(e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        if (!$(this).parsley().validate()) {
            return;
        }
        $("#update_slider_modal").iziModal('startLoading');
        formData = new FormData($(this)[0]);
        // for (var pair of formData.entries()) {
        //     console.log(pair[0] + ', ' + pair[1]);
        // }
        $.ajax({
            url: document.URL,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(res) {
                $("#update_slider_modal").iziModal('stopLoading');
                if (res.ok) {
                    location.reload();
                } else {
                    $msg = '<div class="alert alert-dismissable alert-danger">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        '<strong>' + res.msg + '</strong>' +
                        '</div>';
                    $("#update_msg").html($msg);
                }
            }
        });
    });
</script>