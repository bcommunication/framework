<?php if ($about) : ?>
    <form name="updateAbout" enctype="multipart/form-data" class="form-horizontal form-bordered" method="POST">
        <div id="update_msg"></div>
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-2">Titre</label>
                <div class="col-sm-10">
                    <input name="title" value="<?php echo $about->title; ?>" data-parsley-required data-parsley-required-message="Champ obligatoire" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Texte</label>
                <div class="col-sm-10">
                    <textarea name="text" rows="5" data-parsley-required data-parsley-required-message="Champ obligatoire" type="text" class="form-control"><?php echo $about->text; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Image</label>
                <div class="col-sm-10">
                    <input type="file" name="image" class="form-control">
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-inverse">Envoyer</button>
        </div>
    </form>
    <script src="<?php echo asset_path('/admin/ckeditor/ckeditor.js'); ?>"></script>
    <script>
        CKEDITOR.replace('text', {
            extraPlugins: 'colorbutton,panelbutton,floatpanel,panel,justify,font'
        });
        $('form[name="updateAbout"]').submit(function(e) {
            e.preventDefault();
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $("#update_about_modal").iziModal('startLoading');
            formData = new FormData($(this)[0]);
            formData.append('do', 'updateAbout');
            $.ajax({
                url: document.URL,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result) {
                    $("#update_about_modal").iziModal('stopLoading');
                    if (result.ok) {
                        location.reload();
                    } else {
                        $msg = '<div class="alert alert-dismissable alert-danger">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                            '<strong>' + result.msg + '</strong>' +
                            '</div>';
                        $("#update_msg").html($msg);
                    }
                }
            });
        })
    </script>
<?php else : ?>
    <div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Erreur de récupération des informations!</strong>
    </div>
<?php endif; ?>