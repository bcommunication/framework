<form name="addSlider" enctype="multipart/form-data" class="form-horizontal form-bordered" method="POST">
    <div id="add_msg"></div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-2">Titre</label>
            <div class="col-sm-10">
                <input name="title" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Sous-titre</label>
            <div class="col-sm-10">
                <input name="subtitle" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Texte</label>
            <div class="col-sm-10">
                <textarea name="text" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Image</label>
            <div class="col-sm-10">
                <input type="file" name="image" class="form-control">
                <input type="hidden" name="do" value="addSlide" />
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="btn btn-inverse">Envoyer</button>
    </div>
</form>
<script>
    var ckeditor = CKEDITOR.instances['text'];
    if (ckeditor) {
        ckeditor.destroy(true);
    }
    CKEDITOR.replace('text', {
        extraPlugins: 'colorbutton,panelbutton,floatpanel,panel,justify,font'
    });
    $('form[name="addSlider"]').submit(function(e) {
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        if (!$(this).parsley().validate()) {
            return;
        }
        $("#add_slider_modal").iziModal('startLoading');
        formData = new FormData($(this)[0]);
        $.ajax({
            url: document.URL,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(res) {
                console.log(res.ok);
                $("#add_slider_modal").iziModal('stopLoading');
                if (res.ok) {
                    location.reload();
                } else {
                    var msg =
                        '<div class="alert alert-dismissable alert-danger">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        '<strong>' + res.msg + '</strong>' +
                        '</div>';
                    $("#add_msg").html(msg);
                }
            }
        });
    });
</script>