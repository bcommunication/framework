<!DOCTYPE html>
<html class="backend">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" type="image/x-icon" href="/res/assets/images/logo_circle_icon.ico" />
    <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/bootstrap.css" />
    <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/layout.css" />
    <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/uielement.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css" />
    <script type="text/javascript" src="/framework/res/assets/admin/plugins/modernizr/js/modernizr.js"></script>
    <link rel="stylesheet" href="/framework/res/assets/admin/plugins/iziModal/css/iziModal.css" />
    <title>Intro</title>
</head>
<body>
    <?php view_part('admin_sidebar') ?>
    <section id="main" role="main">
        <div class="page-header page-header-block pb0">
            <div class="pb15">
                <div class="page-header-section">
                    <div class="toolbar clearfix">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-primary add_slider_button"><span class="ico-plus-circle2"></span> Ajouter slide</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="msg">
            </div>
        </div>
            <?php $msg = session_get('msg');
            if ($msg) : ?>
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo $msg; ?></strong>
                </div>
            <?php session_delete('msg');
            endif;
            if ($carousel) : ?>
                <div class="row-flex" id="browse_slides">
                    <?php $i = 1;
                    foreach ($carousel as $s) : ?>
                        <div class="col-xs-12 col-md-6 col-lg-4" id="<?php echo $s->id; ?>">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Ordre : <?php echo $i;
                                                $i++; ?>
                                    </h3>
                                </div>
                                <div class="thumbnail">
                                    <div class="media">
                                        <div>
                                        </div>
                                        <img width="100%" height="auto" src="<?php echo asset_path('/img/slide/' . $s->image); ?>">
                                    </div>
                                </div>
                                <?php if ($s->title || $s->text) : ?>
                                    <div class="panel-body">
                                        <h3 class="panel-title"><b>Titre :</b></h3>
                                        <?php echo $s->title ?>
                                        <hr>
                                        <h3 class="panel-title"><b>Sous-titre :</b></h3>
                                        <?php echo $s->subtitle ?>
                                        <hr>
                                        <h3 class="panel-title"><b>Texte :</b></h3>
                                        <?php echo $s->text ?>
                                    </div>
                                <?php endif; ?>
                                <div class="panel-footer">
                                    <button class="btn btn-primary update_slide" data-id="<?php echo $s->id ?>"><i class="ico-pencil"></i></button></li>
                                    <button class="btn btn-danger del_slide pull-right" data-id="<?php echo $s->id; ?>" data-img="<?php echo $s->image ?>"><i class="ico-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div id="update_slider_modal" class="iziModal"></div>
            <?php else : ?>
                <div class="alert alert-dismissable alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Pas de sliders encore !</strong>
                </div>
            <?php endif; ?>
            <div id="add_slider_modal" class="iziModal"></div>
        </div>
    </section>
    <script type="text/javascript" src="/framework/res/assets/admin/javascript/vendor.js"></script>
    <script type="text/javascript" src="/framework/res/assets/admin/javascript/core.js"></script>
    <script type="text/javascript" src="/framework/res/assets/admin/javascript/app.js"></script>
    <script type="text/javascript" src="/framework/res/assets/admin/plugins/iziModal/js/iziModal.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="/framework/res/assets/admin/plugins/parsley/js/parsley.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="<?php echo asset_path('/admin/ckeditor/ckeditor.js'); ?>"></script>
    <script>
        $loader = '<div id="loader" class="indicator show"></div>';
        // Carousel section
        $('body').on('click', '.del_slide', function() {
            const id = $(this).attr('data-id');
            $("#browse_slides").append($loader);
            $title = 'Confirmez la suppression';
            $msg = 'Êtes-vous sûr de vouloir supprimer ce slide? ';
            $buttons = {
                Supprimer: {
                    action: function() {
                        $.post(document.URL, {
                            id: id,
                            do: 'deleteSlide'
                        }, function(data) {
                            if (data.ok) {
                                location.reload();
                            }
                            $("#loader").remove();
                        })
                    },
                    btnClass: 'btn-danger',
                },
                Annuler: {
                    action: function() {
                        $("#loader").remove();
                    },
                    btnClass: 'btn-primary',
                }
            };
            $.confirm({
                title: $title,
                content: $msg,
                buttons: $buttons
            });
        });
        $("#add_slider_modal").iziModal({
            title: "Ajouter slide",
            icon: 'icon-plus',
            iconColor: 'white',
            fullscreen: true,
            width: 900,
            padding: 20,
            bodyOverflow: false,
            zindex: 50000,
            top: 100,
            bottom: 50
        });
        $("#update_slider_modal").iziModal({
            title: "Modifier slide",
            icon: 'icon-pencil',
            iconColor: 'white',
            fullscreen: true,
            width: 900,
            padding: 20,
            bodyOverflow: false,
            zindex: 50000,
            top: 100,
            bottom: 50
        });
        $(".add_slider_button").click(function() {
            $("#add_slider_modal").iziModal('open');
            $("#add_slider_modal").iziModal('startLoading');
            $.post(document.URL, {
                do: 'getAddForm'
            }, function(data) {
                $("#loader").remove();
                $("#add_slider_modal").iziModal('stopLoading');
                $("#add_slider_modal .iziModal-content").html(data.html);
            });
        });
        $('.update_slide').click(function() {
            $("#update_slider_modal").iziModal('open');
            $("#update_slider_modal").iziModal('startLoading');
            $.post(document.URL, {
                do: 'getUpdateSlideForm',
                id: $(this).attr('data-id')
            }, function(data) {
                $("#loader").remove();
                $("#update_slider_modal").iziModal('stopLoading');
                $("#update_slider_modal .iziModal-content").html(data.html);
            })
        });
    </script>
</body>
</html>