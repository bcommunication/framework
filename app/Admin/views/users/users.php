<!DOCTYPE html>
<html class="backend">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Utilisateurs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/bootstrap.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/layout.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/uielement.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/plugins/gritter/css/gritter.css">
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/modernizr/js/modernizr.js"></script>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    </head>
    <body>
        <?php view_part('admin_sidebar') ?>
        <section id="main" role="main">
            <div class="page-header page-header-block pb0">
                <div class="pb15">
                    <div class="page-header-section">
                        <div class="toolbar clearfix">
                            <div class="col-sm-6">
                                <ul class="nav nav-pills">
                                    <li class="active"><a href="#"><i class="ico-users mr5"></i> utilisateurs</a></li>
                                    <li><a href="<?php echo route_url('users_settings'); ?>"><i class="ico-cog4 mr5"></i> Paramètres</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <form id="search_users" action="<?php echo route_url('browse_users') ?>">
                                <div class="has-icon">
                                    <input name="q" type="search" class="form-control" name="shuffle-filter" id="shuffle-filter" placeholder="Recherche">
                                    <i class="ico-search form-control-icon"></i>
                                </div>
                                    <input type="hidden" name="page" value="1"/>
                                </form>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary" data-toggle="collapse" data-target="#add_user"><span class="ico-plus-circle2"></span> Ajouter</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="collapse" id="add_user" method="post"   data-parsley-validate>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Nom</label>
                                <input type="text" data-parsley-pattern-message='Nom invalide' data-parsley-pattern="^[a-zA-Z]+$" name="nom" data-parsley-required data-parsley-required-message='Champ obligatoire' class="form-control">
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Prenom</label>
                                <input type="text" name="prenom" class="form-control" data-parsley-pattern-message='Prènom invalide' data-parsley-pattern="^[a-zA-Z]+$" data-parsley-required data-parsley-required-message='Champ obligatoire'>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Nom d'utilisateur</label>
                                <input data-parsley-type="alphanum" data-parsley-type-message="Nom d'utilisateur invalide" type="text" name="username" data-parsley-required data-parsley-required-message='Champ obligatoire' class="form-control">
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Email</label>
                                <input type="text" name="email" class="form-control" data-parsley-required data-parsley-type='email' data-parsley-required-message='Champ obligatoire' data-parsley-type-message='Email invalide'>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Mot de passe</label>
                                <input type="password" data-parsley-minlength="6" data-parsley-minlength-message="Au moins 6 charactère" name="pass" data-parsley-required data-parsley-required-message='Champ obligatoire' class="form-control">
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Retapez mot de passe</label>
                                <input type="password" name="repass" data-parsley-equalto="input[name='pass']" data-parsley-equalto-message="Les mots de passe ne sont pas similaires" class="form-control" data-parsley-required data-parsley-required-message='Champ obligatoire'>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Role</label>
                                <select name="role" class="form-control">
                                    <option value="root">Adminstrateur</option>
                                    <option value="comercial">Commercial</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success pull-right">Envoyer</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container-fluid">
                <div class="row" >
                    <div id="main_content">
                    </div>
                    <div id="paging">
                    </div>
                </div>
            </div> 
        </section>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/vendor.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/core.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/app.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/parsley/js/parsley.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/bootbox/js/bootbox.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/gritter/js/jquery.gritter.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <script>
            $loader = '<div id="loader" class="indicator show"><span class="spinner spinner1"></span></div>';
            $(document).ready(function(){
                $("#main_content").append($loader);
                $.get("<?php echo route_url('browse_users'); ?>",{},function(data){
                    $("#main_content").html(data.users);
                    $("#paging").html(data.paging);
                    $("#loader").remove();
                });
            })
            $("#add_user").submit(function(e){
                e.preventDefault();
                if(!$(this).parsley().validate())
                {
                    return;
                }
                $("#add_user").append($loader);
                $.post("<?php echo route_url('add_user'); ?>",$(this).serialize(),function(data){
                    if(data.ok)
                    {
                        $("#main_content").append(data.html)
                    }
                    $("#loader").remove();
                    $.gritter.add({
                        title: 'Notification',
                        text: data.msg,
                        sticky: true,
                    });
                })
                e.preventDefault();
            })
            $('body').on('click','.del_user',function(){
                $id = $(this).attr('id').split('_')[1];
                $this = $(this);
                $.confirm({
               title:'Confirmez la suppression',
               content: 'etes vous sur de vouloir supprimer cet utilisateur?',
                buttons: {
                    Supprimer: {
                        action:function()
                        {
                             $this.append($loader);
                $.post("<?php echo route_url('delete_user') ?>",{id:$id},function(data){
                    if(data.ok)
                    {
                        $("#"+$id).remove();
                    }
                    $("#loader").remove();
                    $.gritter.add({
                        title: 'Notification',
                        text: data.msg,
                        sticky: true,
                    });
                })
                        },
                        btnClass: 'btn-danger',
                    },
                    Annuler: {
                        action:function()
                        {
                        },
                        btnClass: 'btn-primary',
                    }
                }
            });
            })
            $('body').on('click','#more',function(){
                $q = $(this).attr('data-q');
                $page = $(this).attr('data-page');
                $("#paging").append($loader);
                $.get("<?php echo route_url('browse_users'); ?>",{page:$page,q:$q},function(data){
                    $("#main_content").append(data.users);
                    $("#paging").html(data.paging);
                    $("#loader").remove();
                });
            })
            $('#search_users').submit(function(e){
                e.preventDefault();
                if(!$('input[name="q"').val())
                {
                    return;
                }
                $("#main_content").append($loader);
                $.get("<?php echo route_url('browse_users'); ?>",$(this).serialize(),function(data){
                    $("#main_content").html(data.users);
                    $("#paging").html(data.paging);
                    $("#loader").remove();
                });
            })
            $('body').on('click','#back_search',function(){
                $("#main_content").append($loader);
                $.get("<?php echo route_url('browse_users'); ?>",{},function(data){
                    $("#main_content").html(data.users);
                    $("#paging").html(data.paging);
                    $("#loader").remove();
                });
            })
        </script>
    </body>
</html>