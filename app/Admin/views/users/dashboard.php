<!DOCTYPE html>
<html class="backend">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>tableau de bord</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/bootstrap.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/layout.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/uielement.css">
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/modernizr/js/modernizr.js"></script>
    </head>
    <body>
        <?php view_part('admin_sidebar') ?>
        <section id="main" role="main">
            <div class="container-fluid"> 
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
            </div> 
        </section>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/vendor.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/core.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/app.js"></script>
    </body>
</html>