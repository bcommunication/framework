<div id="<?php echo $user['id']; ?>" class="col-sm-6 col-md-4 shuffle">
                        <div class="panel widget">
                            <div class="table-layout nm">
                                <div class="col-xs-4 text-center"><img height="36%" src="<?php echo asset_path('/admin/avatar.gif'); ?>" width="100%"></div>
                                <div class="col-xs-8 valign-middle">
                                    <div class="panel-body">
                                        <button type="button" id="user_<?php echo $user['id']; ?>" class="del_user btn btn-xs btn-danger pull-right"><i class="ico-trash"></i></button>
                                        <h5 class="semibold mt0 mb5"><a href="javascript:void(0);"><?php echo $user['nom'].' '.$user['prenom']; ?></a></h5>
                                        <p class="ellipsis text-muted mb5"><i class="ico-user2 mr5"></i> <?php echo $user['username']; ?></p>
                                        <p class="ellipsis text-muted mb5"><i class="ico-envelop mr5"></i> <?php echo $user['email']; ?></p>
                                        <p class="text-muted nm"><i class="ico-location2 mr5"></i><?php echo $user['role']=='root'?'Administrateur':'Commercial'; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>