<?php if($paging['total']>1&&$paging['actual']!=$paging['total']): ?>
<div class="col-sm-8 col-sm-offset-2">
    <button style="width: 100%" type="button" id="more" data-page="<?php echo $paging['actual']+1 ?>" data-q="<?php echo http_get('q'); ?>" class="btn  btn-inverse btn-outline mb5">Voir plus</button>
</div>
<?php endif; ?>
