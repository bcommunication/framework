<!DOCTYPE html>
<html class="backend">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Connectez-vous</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="/admin/image/favicon.ico') ?>">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/bootstrap.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/layout.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/uielement.css">
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/modernizr/js/modernizr.js"></script>
    </head>
    <body>
        <section id="main" role="main">
            <section class="container">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4" style="margin-top: 9%;">
                        <form class="panel panel-info" method="post" name="form-login" action="">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="ico-lock3 mr5"></i> Connectez-vous</h3>
                            </div>
                            <div class="panel-body">
                                <?php if(isset($response->error)): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <span class="semibold">
                                        <?php if($response->error == 'max_try'): ?>
                                            vous avez essayé trop de fois
                                        <?php else: ?>
                                            mot de passe ou nom d'utilisateur incorrect
                                        <?php endif; ?>
                                    </span>
                                </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input name="email" type="text" class="form-control input-lg" placeholder="Email ou nom d'utilisateur"  value="<?php echo http_post('email'); ?>" data-parsley-errors-container="#error-container" data-parsley-error-message="Nom d'utilisateur / email requis" data-parsley-required>
                                        <i class="ico-user2 form-control-icon"></i>
                                    </div>
                                    <div class="form-stack has-icon pull-left">
                                        <input name="password" type="password" class="form-control input-lg" placeholder="Mot de passe" data-parsley-errors-container="#error-container" data-parsley-error-message="Mot de passe requis" data-parsley-required>
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>
                                <div id="error-container"class="mb15"></div>
                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-block btn-success"><span class="semibold">Se connecter</span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </section>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/vendor.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/core.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/javascript/app.js"></script>
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/parsley/js/parsley.js"></script>      
        <script>
        var $form    = $('form[name=form-login]');
        $form.on('click', 'button[type=submit]', function (e) {
            var $this = $(this);
            if (!$form.parsley().validate())
            {
                $form
                    .removeClass('animation animating shake')
                    .addClass('animation animating shake')
                    .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                        $(this).removeClass('animation animating shake');
                    });
            }
        });
        </script>
    </body>
</html>