<!DOCTYPE html>
<html class="backend">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Compte</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/bootstrap.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/layout.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/stylesheet/uielement.css">
        <link rel="stylesheet" href="/framework/res/assets/admin/plugins/selectize/css/selectize.css"/>
        <script type="text/javascript" src="/framework/res/assets/admin/plugins/modernizr/js/modernizr.js"></script>
    </head>
    <body>
    <?php view_part('admin_sidebar') ?>
    <section id="main" role="main">
        <div class="page-header page-header-block pb0">
            <div class="pb15">
                <div class="page-header-section">
                    <div class="toolbar clearfix">
                        <div class="col-sm-6">
                            <ul class="nav nav-pills">
                                <li><a href="<?php echo route_url('manage_users'); ?>"><i class="ico-users mr5"></i> utilisateurs</a></li>
                                <li class="active"><a href="#"><i class="ico-cog4 mr5"></i> Paramètres</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class=" col-sm-12">
                <form class="panel panel-primary form-horizontal form-bordered" id="main_panel" name="setting" action="" method="POST">
                    <div class="panel-heading"><h5 class="panel-title"><i class="ico-pencil"></i> Modifier compte</h5></div>
                    <div id="msg"></div>
                    <div class="panel-body pt0">
                        <div class="form-group message-container"></div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <input type="text" data-parsley-group="username" value="<?php echo user_account('username') ?>" name="username" class="form-control" placeholder="Nom d'utilisateur" data-parsley-required data-parsley-required-message="Nom d'utilisateur requi" data-parsley-type="alphanum" data-parsley-type-message="Nom d'utilisateur ne peut pas contenir des caractere spéciaux">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" data-parsley-group="username" name="email" value="<?php echo user_account('email') ?>" class="form-control" placeholder="Email" data-parsley-required data-parsley-required-message='Email requis' data-parsley-type='email' data-parsley-type-message='Email invalide'>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <input type="text" data-parsley-group="username" value="<?php echo user_account('nom') ?>" name="nom" class="form-control" placeholder="Nom" data-parsley-required data-parsley-required-message="Nom  requi" data-parsley-type="alphanum" data-parsley-type-message="Nom  ne peut pas contenir des caractere spéciaux">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" data-parsley-group="username" value="<?php echo user_account('prenom') ?>" name="prenom" class="form-control" placeholder="Prenom" data-parsley-required data-parsley-required-message="Prenom  requi" data-parsley-type="alphanum" data-parsley-type-message="Prenom  ne peut pas contenir des caractere spéciaux">
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-info ladda-button" id="submit_username" ><span class="ladda-label">Modifier</span></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <input type="password" data-parsley-group="pass" data-parsley-minlength="6" data-parsley-minlength-message='Au moins 6 caractere' data-parsley-required data-parsley-required-message='Mot de passe requis' name="old_pass" class="form-control" placeholder="Ancienne mot de passe">
                            </div>
                            <div class="col-sm-3">
                                <input type="password" data-parsley-group="pass" data-parsley-required data-parsley-required-message='Retapez mot de passe' data-parsley-minlength="6" data-parsley-minlength-message='Au moins 6 caractere' name="new_pass" class="form-control" placeholder="Nouvelle mot de passe">
                            </div>
                            <div class="col-sm-3">
                                <input type="password" data-parsley-group="pass" data-parsley-required data-parsley-required-message='Retapez mot de passe' data-parsley-equalto="input[name='new_pass']" data-parsley-equalto-message='Mot de passe non identique' name="repass" class="form-control" placeholder="Retapez mot de passe">
                            </div>
                            <div class="col-sm-2">
                                <button type="button"  class="btn btn-info ladda-button" id="submit_password"><span class="ladda-label">Modifier</span></button>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        </div>
    </section>
    <script type="text/javascript" src="/Comparons/res/assets/admin/javascript/vendor.js"></script>
    <script type="text/javascript" src="/Comparons/res/assets/admin/javascript/core.js"></script>
    <script type="text/javascript" src="/Comparons/res/assets/admin/javascript/app.js"></script>
    <script type="text/javascript" src="/Comparons/res/assets/admin/plugins/parsley/js/parsley.js"></script>
    <script>
        $loader = '<div id="loader" class="indicator show"><span class="spinner spinner1"></span></div>';
        $("#submit_username").click(function(){
            if(!$("form[name='setting']").parsley().validate('username'))
            {
                return false;
            }
            $("#main_panel").append($loader);
            $.post(document.URL,{do:'username',nom:$('input[name="nom"]').val(),prenom:$('input[name="prenom"]').val(),email:$('input[name="email"]').val(),username:$('input[name="username"]').val()},function(data){
                if(data.ok)
                {
                    $msg = '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Compte modifiée</strong></div>';
                }
                else
                {
                    $msg = '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Nom d\'utilisateur / email existe ou vous n\'avez rien changé</strong></div>';
                }
                $("#msg").html($msg);
                $("#loader").remove();
            })
        });
        $("#submit_password").click(function(){
            if(!$("form[name='setting']").parsley().validate('pass'))
            {
                return false;
            }
            $("#main_panel").append($loader);
            $.post(document.URL,{do:'password',old:$('input[name="old_pass"]').val(),new:$('input[name="new_pass"]').val(),repass:$('input[name="repass"]').val()},function(data){
                if(data.ok)
                {
                    $msg = '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Compte modifiée</strong></div>';
                }
                else
                {
                    $msg = '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Ancienne mot de passe invalide ou vous n\'avez rien changé</strong></div>';
                }
                $("#msg").html($msg);
                $("#loader").remove();
            })
        });
    </script>
    </body>
</html>