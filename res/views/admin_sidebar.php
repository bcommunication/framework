<header id="header" class="navbar">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo route_url('dashboard'); ?>">
                    <span style="color: white;">App</span>
                </a>
            </div>
            <div class="navbar-toolbar clearfix">
                <ul class="nav navbar-nav navbar-left">
                    
                    <li class="hidden-xs hidden-sm">
                        <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                            <span class="meta">
                                <span class="icon"></span>
                            </span>
                        </a>
                    </li>
                    <li class="navbar-main hidden-lg hidden-md hidden-sm">
                        <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                            <span class="meta">
                                <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                            </span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown profile">
                        <a href="<?php echo route_url('logout',['token'=> user_from_session('login_token')]); ?>" class="dropdown-hover">
                            <b>Deconnexion</b>
                        </a>
                        
                    </li>
                </ul>
            </div>
        </header>
            <aside class="sidebar sidebar-left sidebar-menu">
                    <section class="content slimscroll">
                        <ul class="topmenu topmenu-responsive" data-toggle="menu">
                            <li>
                                <a target="_blank" href="<?php echo route_url('front_accueil') ?>"  data-parent=".topmenu">
                                    <span class="figure"><i class="ico-globe"></i></span>
                                    <span class="text">Accéder au site</span>
                                </a>
                            </li>
                            <?php if(user_is_root()): ?>
                            <li <?php if(in_array(route_name(), ['manage_users','users_settings'])): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo route_url('manage_users') ?>"  data-parent=".topmenu">
                                    <span class="figure"><i class="ico-dashboard"></i></span>
                                    <span class="text">Administration</span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <hr>
                            <li <?php if (route_name() == 'admin_preference') : ?> class="active" <?php endif; ?>>
                                <a href="" data-parent=".topmenu">
                                    <span class="figure"><i class="ico-globe"></i></span>
                                    <span class="text">Préférences</span>
                                </a>
                            </li>
                            <li <?php if (route_name() == 'admin_slider') : ?> class="active" <?php endif; ?>>
                                <a href="<?php echo route_url('admin_slider') ?>" data-parent=".topmenu">
                                    <span class="figure"><i class="ico-image"></i></span>
                                    <span class="text">Intro</span>
                                </a>
                            </li>
                        </ul>
                    </section>
            </aside>