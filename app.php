<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('Pacific/Tahiti');
error_reporting(E_ALL);
use app\Parameter as Config;
use Falf\Container;
use Falf\NotFoundException;
define('PROJECT_DIR', __DIR__);
define('DS', DIRECTORY_SEPARATOR);
define('STOP_EXECUTION', 11);
define('CONTINUE_EXECUTION', 12);
function user_crypt_pass($password)
{
    return Falf\Users\User::cryptPassword($password);
}
function number_format_short( $n, $precision = 1 ) {
	if ($n < 900) {
		// 0 - 900
		$n_format = number_format($n, $precision);
		$suffix = '';
	} else if ($n < 900000) {
		// 0.9k-850k
		$n_format = number_format($n / 1000, $precision);
		$suffix = 'K';
	} else if ($n < 900000000) {
		// 0.9m-850m
		$n_format = number_format($n / 1000000, $precision);
		$suffix = 'M';
	} else if ($n < 900000000000) {
		// 0.9b-850b
		$n_format = number_format($n / 1000000000, $precision);
		$suffix = 'B';
	} else {
		// 0.9t+
		$n_format = number_format($n / 1000000000000, $precision);
		$suffix = 'T';
	}
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
	if ( $precision > 0 ) {
		$dotzero = '.' . str_repeat( '0', $precision );
		$n_format = str_replace( $dotzero, '', $n_format );
	}
	return $n_format . $suffix;
}
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'ans',
        'm' => 'mois',
        'w' => 'semaines',
        'd' => 'jours',
        'h' => 'heurs',
        'i' => 'minutes',
        's' => 'seconds',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? 'depuit '.implode(', ', $string) : 'Maintenant';
}

function password($add_dashes = false, $available_sets = 'luds') {
        $l = [8, 9, 10, 11, 12, 13, 14, 15];
        $length = $l[array_rand($l)];
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
//array functions
function recursive_array_set($key,$value,&$array)
    {
        $k = explode('.', $key);
        $target = &$array;
        foreach ($k as $v)
        {
            if(!isset($target[$v]))
            {
                $target[$v] = [];
            }
            $target = &$target[$v];
        }
        $target = $value;
    }

function recursive_array_get($key,&$array)
    {
        $k = explode('.', $key);
        $target = &$array;
        foreach ($k as $v)
        {
            if(!isset($target[$v]))
            {
                return FALSE;
            }
            $target = &$target[$v];
        }
        return $target;
}

function recursive_array_has($key,&$array)
    {
        $k = explode('.', $key);
        $target = &$array;
        foreach ($k as $v)
        {
            if(!isset($target[$v])||  is_null($target[$v]))
            {
                return FALSE;
            }
            $target = &$target[$v];
        }
        return TRUE;
    }
//coder functions
function coder_updateConst($const, $value, $class) {
    $file = (new \ReflectionClass($class))->getFileName();
    $pattern = "#const " . $const . " =([^;]*);#s";
    $replacement = "const " . $const . " = " . var_export($value, TRUE) . " ;";
    return file_put_contents($file, preg_replace($pattern, $replacement, file_get_contents($file)));
}

function coder_updateConsts(array $consts, $class) {
    $file = (new \ReflectionClass($class))->getFileName();
    $content = file_get_contents($file);
    foreach ($consts as $k => $v) {

        $pattern = "#const " . $k . " =([^;]*);#s";
        $replacement = "const " . $k . " = " . var_export($v, TRUE) . " ;";
        $content = preg_replace($pattern, $replacement, $content);
    }
    return file_put_contents($file, $content);
}
//http functions
function http_method($method = NULL)
{
    $meth = $_SERVER['REQUEST_METHOD'];
    $m = $meth?strtolower($meth):'any';
    unset($meth);
    if($method)
    {
        return strtolower($m) === strtolower($method);
    }
    return strtolower($m);
}

function http_get($key = NULL)
{
    $g = filter_input_array(INPUT_GET);
    if($key)
    {
        return recursive_array_get($key,$g );
    }
    return filter_input_array(INPUT_GET);
}

function http_post($key = NULL)
{
    $p = filter_input_array(INPUT_POST);
    if($key)
    {
        return recursive_array_get($key, $p);
    }
    return (array)$p;
}

function http_post_delete($key)
{
    if(is_array($key))
    {
        foreach ($key as $v)
        {
            if(isset($_POST[$v]))
                unset ($_POST[$v]);
        }
        return;
    }
    if(isset($_POST[$key]))
        unset ($_POST[$key]);
}

function http_get_delete($key)
{
    if(is_array($key))
    {
        foreach ($key as $v)
        {
            if(isset($_GET[$v]))
                unset ($_GET[$v]);
        }
        return;
    }
    if(isset($_GET[$key]))
        unset ($_GET[$key]);
}

function path_info()
{
    return isset($_SERVER['PATH_INFO'])?$_SERVER['PATH_INFO']:'/';
}


function rewrite_response($url,$code = 302)
{
    Config::after();
    http_response_code($code);
    header('location:'.$url);
    die;
}
function current_url()
{
    return sprintf("%s://%s%s",isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',$_SERVER['SERVER_NAME'],$_SERVER['REQUEST_URI']);
}
function json_response($data)
{
    Config::after();
    header('Content-Type: application/json');
    echo json_encode($data);
    die;
}
//users function

function user_table()
{
    return Falf\Users\User::TABLE_NAME;
}
function user_id()
{
    return Falf\Users\User::fromSession('id');
}
function user_crypt_password($password)
{
    return Falf\Users\User::cryptPassword($password);
}
function user_from_session($index = NULL)
{
    return Falf\Users\User::fromSession($index);
}

function user_account($index = NULL)
{
    return Falf\Users\User::getAccount($index);
}
function user_role()
{
    return \Falf\Users\Acl::getInstance()->getRole();
}

function user_is_role($role)
{
    return \Falf\Users\Acl::getInstance()->isRole($role);
}

function user_is_root()
{
    return \Falf\Users\Acl::getInstance()->isRoot();
}

function user_is_guest()
{
    return \Falf\Users\Acl::getInstance()->isGuest();
}
function user_can($required)
{
    return \Falf\Users\Acl::getInstance()->userCan($required);
}
function get_roles()
{
    return array_keys(Container::get('acl'));
}
function role_exists($role)
{
    $roles = get_roles();
    return in_array($role,$roles);
}
/**
 * 
 * @return \Falf\Users\Acl
 */
function get_acl($new = FALSE)
{
    if($new)
    {
        return new \Falf\Users\Acl();
    }
    return \Falf\Users\Acl::getInstance();
}
//end users function

function url_args($index = NULL)
{   
    if($index)
    {
        return Container::$_matched['args'][$index];
    }
    return Container::$_matched['args'];
}
function route_name()
{
    return Container::$_matched['name'];
}
function route_call($name,$args = [])
{
    $r = Container::getRoutes();
    $route = $r[$name];
    unset($r);
    call_user_func_array([Container::$_controllers[$route['controller']], $route['method']], $args);
}

function print_if_route_in($str,array $in)
{
    $name = current_route('name');
    if(in_array($name, $in))
    {
        echo $str;
    }
}

function current_route($index = NULL)
{
    if(!$index)
    {
        return Container::$_matched;
    }
    return Container::$_matched[$index];
    
}
function get_route($name,$index = NULL)
{
    $r = Container::getRoutes();
    if(!$index)
    {
        return $r[$name][$index];
    }
    return $r[$name][$index];
}

function route_url($name,$args = [])
{
    $route = Container::$_routes[$name];
    if(!isset($route['args']))
    {
        return Config::BASE_DIR.$route['base'];
    }
    $args = array_merge($route['args'],$args);
    $x =  preg_replace_callback('/:(\w+)/', function($mt) use($args){
    return $args[$mt[1]];
    }, $route['base']);
    return Config::BASE_DIR.$x;
}
function route_rewrite($name,$args = [],$code = 302)
{
    rewrite_response(route_url($name,$args), $code);
}
//session functions
function start_session()
{
        if(isset($_COOKIE[session_name()])&&!$_COOKIE[session_name()])
        {
            setcookie(session_name(), NULL,-1);
            return;
        }
        if(session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
}

function session_get($key)
{
    start_session();
    return recursive_array_get($key, $_SESSION);
}
function session_set($key,$value)
{
    start_session();
    recursive_array_set($key, $value, $_SESSION);
}


function session_delete($key)
{
    start_session();
    recursive_array_set($key, NULL, $_SESSION);
}

function session_has($key)
{
    start_session();
    return recursive_array_has($key, $_SESSION);
}
//string functions
function string_startWith($string, $with) {
    return $with === "" || strrpos($string, $with, -strlen($string)) !== false;
}

function string_endWith($string, $with)
{
        return $with === "" || (($temp = strlen($string) - strlen($with)) >= 0 && strpos($string, $with, $temp) !== false);
}
//security functions
function csrf($name)
{
        $token = uniqid(md5(time()));
        session_set('csrf_'.$name, ['phrase'=>$token,'try'=>0]);
        return $token;
}
function captcha($name)
{
        $builder = Gregwar\Captcha\CaptchaBuilder::create();
        $builder->setTextColor(250, 100, 154);
        $builder->setBackgroundColor(0, 0, 0)->build();
        session_set('captcha_'.$name, ['phrase'=>$builder->getPhrase(),'try'=>0]);
        return $builder->inline();
}
//views and assets functions
function view_load($view,$data = [])
{
    Config::after();
    $view = explode('.', $view);
    $global_path = PROJECT_DIR.DS.'res'.DS.'views'.DS.  implode(DS, $view).'.php';
    extract($data);
    if(file_exists($global_path))
    {
        ob_start();
        require_once $global_path;
        echo ob_get_clean();
        die;
    }
    $module = $view[0];
    unset($view[0]);
    ob_start();
    require_once  PROJECT_DIR.DS.'app'.DS.  ucfirst($module).DS.'views'.DS.  implode(DS, $view).'.php';
    echo ob_get_clean();
    die;
}

function view_part($view,$data = [])
{
    $view = explode('.', $view);
    $global_path = PROJECT_DIR.DS.'res'.DS.'views'.DS.  implode(DS, $view).'.php';
    extract($data);
    if(file_exists($global_path))
    {
        require_once $global_path;
        return;
    }
    $module = $view[0];
    unset($view[0]);
    require_once  PROJECT_DIR.DS.'app'.DS.  ucfirst($module).DS.'views'.DS.  implode(DS, $view).'.php';
}

function view_content($view,$data = [])
{
    $view = explode('.', $view);
    extract($data);
    $module = $view[0];
    unset($view[0]);
    ob_start();
    require  PROJECT_DIR.DS.'app'.DS.  ucfirst($module).DS.'views'.DS.  implode(DS, $view).'.php';
    return ob_get_clean();
}



function print_if($str,$bool)
{
    if($bool)
    {
        echo $str;
    }
}
function print_http_post_value($key)
{
    echo 'value="'.  http_post($key).'"';
}


function asset_path($path,$print = TRUE)
{
    $path = Config::BASE_DIR.'/res/assets'.$path;
    if(!$print)
    {
        return $path;
    }
    echo $path;
}


//start lang functions
function get_lang()
{
    $l = session_get('framework.lang');
    if(!$l)
    {
        
        $l = substr(filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'), 0, 2);
        $l = in_array($l,  Config::$langs)?$l:Config::DEFAULT_LANG;
    }
    return $l;
}

function set_lang($lang = NULL)
{
    $l = $lang?$lang:Config::DEFAULT_LANG;
    session_set('framework.lang', $l);
}

function load_lang($path,$value)
{
    $l = Container::get($path);
    if(!$l)
    {
        $e = explode('.', $path);
        $p = PROJECT_DIR.DS.'app'.DS.ucfirst($e[0]).DS.'res'.DS.'langs'.DS.get_lang().DS.implode(DS, array_slice($e, 1)).'.php';
        $l = require $p;
        unset($p);
        Container::set($path,$l);
    }
    return recursive_array_get($value, $l);
    
}

//end lang functions

/**
 * 
 * @param array $data
 * @return Falf\Validator
 */
function validator($data = NULL)
{
    return new Falf\Validator($data);
}
function birth()
{
    $min = strtotime("47 years ago");
        $max = strtotime("18 years ago");
        $rand_time = mt_rand($min, $max);
        return (object)[
            'day'=> date('d',$rand_time),
            'month'=> date('m',$rand_time),
            'year'=> date('Y',$rand_time)
            ];
}

function generate_profile($country = NULL)
{
     set_time_limit(0);
        $sql = \Falf\Container::getSql();
        $q = 'SELECT id,code,lang FROM bot_countrys ';
        if($country)
        {
            $q.=' WHERE code = "'.$country.'" LIMIT 1 ';
        }
        else
        {
            $q.=' order by rand() limit 1';
        }
        $country = $sql->sql($q)->find();
        $profile = $sql->sql('SELECT (SELECT name FROM bot_f_names WHERE country_id = ' . $country->id . ' order by rand() limit 1) first_name,'
                        . ' (SELECT name FROM bot_l_names WHERE country_id = ' . $country->id . ' order by rand() limit 1) last_name,'
                        . ' (SELECT name FROM bot_universitys WHERE country_id = ' . $country->id . ' order by rand() limit 1) university,'
                        . ' (SELECT name FROM bot_jobs WHERE country_id = ' . $country->id . ' order by rand() limit 1) job')->find();
        $session = new \stdClass();
        $session->country = $country->code;
        $session->lang = $country->lang;
        $regions = \Jasny\ISO\CountrySubdivisions::getList($session->country);
        $session->region = $regions?$regions[array_rand($regions)]:'';
        $session->nom = ucfirst(strtolower($profile->first_name));
        $session->prenom = ucfirst(strtolower($profile->last_name));
        $session->university = $profile->university;
        $session->job = $profile->job;
        $birth = birth();
        $session->birth_day = $birth->day;
        $session->birth_month = $birth->month;
        $session->birth_year = $birth->year;
        $session->pass = password();
        $session->browser = app\Bot\Driver::getRandomBrowser();
        $session->date_created = time();
        return $session;
}
/**
 * @return \Falf\Sql
 */
function get_sql()
{
    return \Falf\Container::getSql();
}
/**
 * 
 * @return app\Bot\Sessions\Driver
 */
function get_driver($old = FALSE)
{
    return \Falf\Container::getDriver($old);
}

function set_driver($driver)
{
    \Falf\Container::setDriver($driver);
}

function get_profile($country = NULL)
{
    return \Falf\Container::getProfile($country);
}

function set_profile($profile)
{
    \Falf\Container::setProfile($profile);
}
function kill_driver()
{
    \Falf\Container::killDriver();
}

function run_framework() {
    
    $routes = Container::getRoutes();
    foreach ($routes as $k => $index) {
        $p = path_info();
        if ($index['count'] != count(explode('/', $p))) {
            continue;
        }
        if (!in_array('any', $index['methods']) && !in_array(http_method(), $index['methods'])) {
            continue;
        }
        if (preg_match('#' . $index['regex'] . '#', $p, $args)) {
            array_shift($args);
            $args = $args?array_combine(array_keys($index['args']), $args):[];
            Container::$_matched = ['name' => $k, 'controller' => $index['controller'], 'method' => $index['method'], 'args' => $args];
            break;
        }
    }
    $matched = Container::$_matched;
    if (!$matched) {
        throw new NotFoundException;
    }
    Config::first();
    call_user_func_array([Container::object($matched['controller']), $matched['method']], $matched['args']);
}

spl_autoload_register(function($class){
    $path = implode(DS, explode('\\',$class));
    $p1 = PROJECT_DIR.DS.'vendor'.DS.$path.'.php';
    if(file_exists($p1))
    {
        require_once $p1;
        return;
    }
    $p2 = PROJECT_DIR.DS.DS.$path.'.php';
    if(file_exists($p2))
    {
        require_once $p2;
    }
});

//start set routes
Container::setRoutes(array (
  'admin_slider' => 
  array (
    'methods' => 
    array (
      0 => 'get',
      1 => 'post',
    ),
    'base' => '/intro',
    'regex' => '^/intro$',
    'count' => 2,
    'controller' => 'app\Admin\Intro',
    'method' => 'main',
  ),
  'login' => 
  array (
    'methods' => 
    array (
      0 => 'get',
      1 => 'post',
    ),
    'base' => '/users/login',
    'regex' => '^/users/login$',
    'count' => 3,
    'controller' => 'app\Admin\Users',
    'method' => 'login',
  ),
  'add_user' => 
  array (
    'methods' => 
    array (
      0 => 'post',
    ),
    'base' => '/add-user',
    'regex' => '^/add-user$',
    'count' => 2,
    'controller' => 'app\Admin\Users',
    'method' => 'add',
  ),
  'delete_user' => 
  array (
    'methods' => 
    array (
      0 => 'post',
    ),
    'base' => '/delete-user',
    'regex' => '^/delete-user$',
    'count' => 2,
    'controller' => 'app\Admin\Users',
    'method' => 'delete',
  ),
  'browse_users' => 
  array (
    'methods' => 
    array (
      0 => 'get',
    ),
    'base' => '/browse-users',
    'regex' => '^/browse-users$',
    'count' => 2,
    'controller' => 'app\Admin\Users',
    'method' => 'browse',
  ),
  'manage_users' => 
  array (
    'methods' => 
    array (
      0 => 'get',
    ),
    'base' => '/users',
    'regex' => '^/users$',
    'count' => 2,
    'controller' => 'app\Admin\Users',
    'method' => 'manage',
  ),
  'users_settings' => 
  array (
    'methods' => 
    array (
      0 => 'get',
      1 => 'post',
    ),
    'base' => '/users/settings',
    'regex' => '^/users/settings$',
    'count' => 3,
    'controller' => 'app\Admin\Users',
    'method' => 'settings',
  ),
  'logout' => 
  array (
    'methods' => 
    array (
      0 => 'get',
      1 => 'post',
    ),
    'base' => '/users/logout/:token',
    'regex' => '^/users/logout/([^/]+)$',
    'count' => 4,
    'controller' => 'app\Admin\Users',
    'method' => 'logout',
    'args' => 
    array (
      'token' => false,
    ),
  ),
  'dashboard' => 
  array (
    'methods' => 
    array (
      0 => 'get',
      1 => 'post',
    ),
    'base' => '/admin',
    'regex' => '^/admin$',
    'count' => 2,
    'controller' => 'app\Admin\Users',
    'method' => 'dashboard',
  ),
  'front_accueil' => 
  array (
    'methods' => 
    array (
      0 => 'get',
      1 => 'post',
    ),
    'base' => '/',
    'regex' => '^/$',
    'count' => 2,
    'controller' => 'app\Front\controller\Index',
    'method' => 'main',
  ),
));
//end set routes


try {
    run_framework();    
} catch (Exception $ex) {
    Config::error($ex);
}


